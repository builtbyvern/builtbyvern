<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Built By Vern</title>
	<meta name="description" content="Vern (John &ldquo;Vern&rdquo; Briggs), of Built By Vern, is a front-end developer and web designer based out of Boise, Idaho determined to make Seattle his home.">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="http://builtbyvern.com/favicon.png" />

	<!-- Place favicon.ico in the root directory -->
	<link rel="stylesheet" href="/assets/css/style.css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

</head>

<body class="site">

<?PHP

function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


$user_ip = getUserIP();



?>

	<main>
		<div class="center">
			<div class="wrapper animated">
				<img class="logo" src="/assets/images/logo.svg">
				<ul class="icons">
					<li><a href="https://www.linkedin.com/in/therealjbriggs" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
					<li><a href="https://bitbucket.org/builtbyvern/" target="_blank"><i class="fa fa-bitbucket"></i></a></li>
				</ul>
				<div class="animate-in">

					<h3>
						<strong>Hello, you.</strong>
					</h3>
					<p>
						<strong>My name is Vern, (well, that's what I go by... John is my real name). I&rsquo;m a front-end developer and web designer</strong>
						thriving in Tacoma, Washington. Rumor has it that I&rsquo;m one
						of the rare developers people actually enjoy working with.
						I&rsquo;ve never met a design I couldn&rsquo;t wrangle, work
						beautifully in a team (git and semantic markup, anyone?) and
						I&rsquo;ve won more awards than you can count on a three-fingered hand.
					</p>
					<p>
						<a target="blank" href="/assets/images/vern-resume.pdf">Review my resume</a>, <a target="_blank" href="http://growideashere.com">scroll</a> <a href="http://alzarschool.org">through</a> <a target="_blank" href="http://visitsouthwestidaho.org/">sites</a> <a target="_blank" href="http://milliganevents.com/">I&rsquo;ve</a> <a target="_blank" href="http://ioga.org/">built</a> or just <a href="mailto:john@builtbyvern.com">send me a message</a>.
					</p>
					<p>
						I&rsquo;d love to talk.
					</p>
				</div>
			</div>




		</div>
	</main>





	<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
	<script>window.jQuery || document.write('<script src="/assets/javascript/vendor/jquery-2.2.3.min.js"><\/script>')</script>
	<script type="text/javascript" src="/assets/javascript/main.js"></script>


	<script type="text/javascript">
  !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.1.0";
  analytics.load("AMFUxEeC9cEzrWcNzrgjPqZCuXgQqDf5");
  analytics.page()
  }}();
</script>

<a style="opacity: 0; visibility: hidden; display:none;" title="Google Analytics Alternative" href="http://clicky.com/100944529"><img alt="Google Analytics Alternative" src="//static.getclicky.com/media/links/badge.gif" border="0" /></a>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100944529); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100944529ns.gif" /></p></noscript>
</body>
</html>
